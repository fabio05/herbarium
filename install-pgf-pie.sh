#!/bin/sh
#
# Run this script if LaTeX fails to find 'pgf-pie.sty'
#
PKG_HOME=~/texmf/tex/generic
CTAN_MIRROR=https://mirrors.ctan.org/graphics/pgf/contrib
ZIP_FILE=pgf-pie.zip

echo 'Installing missing pie chart macros for PGF/TikZ (pgf-pie.sty) ...'
mkdir -p "${PKG_HOME}"
cd "${PKG_HOME}"
wget -q ${CTAN_MIRROR}/${ZIP_FILE}
unzip ${ZIP_FILE} -x pgf-pie/demo/*
rm ${ZIP_FILE}
echo "Pie chart macros installed at: ${PKG_HOME} ✔"
