"""
Plant database model.
"""
import sqlite3

from pathlib import Path

REPO_HOME = Path(__file__).resolve().parent.parent
DATABASE_FILE = REPO_HOME / 'herbarium.sqlite'
DATABASE_MEDIA = REPO_HOME / 'media'


def ensure_database_exists():
    """Let's create a database if none exists yet"""
    if not DATABASE_FILE.exists():
        create_database()


def create_database():
    """Create a simple database"""

    query = """\
            CREATE TABLE plant (
                id INTEGER PRIMARY KEY,
                name TEXT,
                scientific TEXT,
                description TEXT,
                leaf_image TEXT,
                catalog TEXT
            )
            """

    with sqlite3.connect(DATABASE_FILE) as connection:
        cursor = connection.cursor()
        cursor.execute(query)


def get_database_records(catalog=None):
    """Read the data we have stored in our database"""

    where = f"WHERE catalog = '{catalog}'" if catalog else ""

    query = f"SELECT id, name, scientific, description, leaf_image, catalog " \
            f"FROM plant " \
            f"{where} " \
            f"ORDER BY catalog, name"

    with sqlite3.connect(DATABASE_FILE) as connection:
        # fetch dicts instead of tuples
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        return rows


def get_database_item(plant_id):
    """Return a single data item stored in our database"""

    query = f"SELECT id, name, scientific, description, leaf_image, catalog " \
            f"FROM plant " \
            f"WHERE id = '{plant_id}'"

    with sqlite3.connect(DATABASE_FILE) as connection:
        # fetch dicts instead of tuples
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute(query)
        item = cursor.fetchone()
        return item


def create_database_item():
    """Create a new (empty) database record"""

    insert_query = f"INSERT INTO plant (name) VALUES ('_')"

    with sqlite3.connect(DATABASE_FILE) as connection:
        # fetch dicts instead of tuples
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        cursor.execute(insert_query)

        select_query = f"SELECT id FROM plant WHERE rowid = '{cursor.lastrowid}'"
        cursor.execute(select_query)
        item_id = cursor.fetchone()['id']
        return item_id


def update_database_item(plant_id, name, scientific, description, leaf_image, catalog):
    """Update a single database record"""

    mandatory_fields = {
        'name': name,
        'description': description,
        'catalog': catalog,
    }
    optional_fields = {
        'scientific': scientific,
        'leaf_image': leaf_image.filename,
    }
    set_clause = ""

    for field, value in mandatory_fields.items():
        set_clause += f", {field}='{value}'"

    for field, value in optional_fields.items():
        if value:
            set_clause += f", {field}='{value}'"

    query = f"UPDATE plant " \
            f"SET {set_clause[2:]} " \
            f"WHERE id = '{plant_id}'"

    with sqlite3.connect(DATABASE_FILE) as connection:
        cursor = connection.cursor()
        cursor.execute(query)


def delete_database_item(plant_id):
    """Delete a single database item"""

    query = f"DELETE FROM plant " \
            f"WHERE id = '{plant_id}'"

    with sqlite3.connect(DATABASE_FILE) as connection:
        cursor = connection.cursor()
        cursor.execute(query)
