"""
Plant database application.
"""
import imghdr
import sentry_sdk
import subprocess

from pylatex.utils import escape_latex
from sentry_sdk.integrations.flask import FlaskIntegration
from shutil import rmtree
from subprocess import CalledProcessError

from flask import (
    abort,
    Flask,
    redirect,
    render_template,
    request,
    url_for,
)
from werkzeug.utils import secure_filename

from model import (
    create_database_item,
    delete_database_item,
    ensure_database_exists,
    get_database_item,
    get_database_records,
    update_database_item,
    DATABASE_FILE,
    DATABASE_MEDIA,
    REPO_HOME,
)

sentry_sdk.init(integrations=[FlaskIntegration()])

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 7 * 1024 * 1024  # 7 MiB


@app.before_first_request
def initialize():
    ensure_database_exists()


@app.get('/')
def home():
    records = get_database_records()
    return render_template('list.html', plants=records)


@app.get('/new')
def new():
    auth_user = request.environ.get('REMOTE_USER')
    return render_template('detail.html', user=auth_user)


@app.get('/edit/<int:plant_id>')
def edit(plant_id):
    item = get_database_item(plant_id)
    return render_template('detail.html', plant=item)


@app.post('/create')
def create():
    """Create an empty item first, then set the details of it."""
    plant_id = create_database_item()
    return update(plant_id)


@app.post('/update/<int:plant_id>')
def update(plant_id):
    name = request.form.get('name')
    scientific = request.form.get('scientific')
    description = request.form.get('description')
    leaf_image = request.files.get('leaf_image')
    catalog = request.form.get('catalog')

    leaf_image = save_image(plant_id, leaf_image)
    update_database_item(plant_id, name, scientific, description, leaf_image, catalog)

    return redirect(url_for('home'))


@app.get('/delete/<int:plant_id>')
def delete(plant_id):
    delete_database_item(plant_id)
    delete_images(plant_id)
    return redirect(url_for('home'))


@app.get('/generate')
def generate_latex():
    """Generate LaTeX documents from our database content."""
    auth_user = request.environ.get('REMOTE_USER', 'unknown user').capitalize()
    system = print if app.debug else run
    system("git pull")

    for catalog in ['Fabio', 'Jafet']:
        records = get_database_records(catalog)
        plants = convert_to_latex(records)
        document = render_template('catalog.tex', catalog=catalog, plants=plants)
        filename = REPO_HOME / 'chapters' / f'katalog-{catalog.lower()}.tex'

        with open(filename, 'w') as latex_file:
            latex_file.write(document)

        system(f'git add -v {filename}')

    try:
        system(f"git add -v {DATABASE_FILE.name} {DATABASE_MEDIA.name}")
        system(f"git commit -m 'Document generation triggered by {auth_user}'")
        system(f"git push")
        return redirect(url_for('home'))
    except CalledProcessError as err:
        sentry_sdk.capture_exception(err)
        abort(500, "Git failed to commit or push your code. Sorry!")


def convert_to_latex(records):
    """Escape latex control characters in raw data."""
    data_list = []

    for row in records:
        data = dict()
        data['id'] = row['id']
        data['name'] = escape_latex(row['name'])
        data['scientific'] = escape_latex(row['scientific'])
        paragraph = escape_latex(row['description'])
        data['description'] = split_long_lines(paragraph)
        data['leaf_image'] = row['leaf_image']
        data['catalog'] = row['catalog']

        data_list.append(data)

    return data_list


def split_long_lines(line, max_words=15):
    """Split up lines that are too long into several ones."""
    tokens = line.split()
    paragraph = ""

    while tokens:
        head, tokens = tokens[:max_words], tokens[max_words:]
        paragraph += "\n" + " ".join(head)

    return paragraph.strip()


def run(command):
    """Better version of os.system(), throws an exception on error."""
    subprocess.run(command, cwd=REPO_HOME, shell=True, check=True)


def is_valid_image(stream, allowed=['jpeg', 'png']):
    """Verify that the uploaded document is an image."""
    header = stream.read(512)
    stream.seek(0)
    format = imghdr.what(None, header)
    return format in allowed


def save_image(plant_id, file_storage):
    """Write the uploaded file to disk."""
    if file_storage.filename:
        if not is_valid_image(file_storage.stream):
            abort(400, "File type not supported. Must be JPEG or PNG.")
        upload_path = DATABASE_MEDIA / f'{plant_id}'
        upload_path.mkdir(mode=0o755, exist_ok=True)
        file_storage.filename = secure_filename(file_storage.filename)
        file_storage.save(upload_path / file_storage.filename)

    return file_storage


def delete_images(plant_id):
    """Remove the entire media folder for a database record."""
    upload_path = DATABASE_MEDIA / f'{plant_id}'
    rmtree(upload_path, ignore_errors=True)


if __name__ == '__main__':
    app.run(debug=True)
